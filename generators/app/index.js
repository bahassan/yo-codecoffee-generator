'use strict';
var generators = require('yeoman-generator');
var yosay = require('yosay');
var chalk = require('chalk');
var wiredep = require('wiredep');
var mkdirp = require('mkdirp');
var _s = require('underscore.string');

var CodeCoffeeGenerator = generators.Base.extend({
    constructor: function () {
        var testLocal;

        generators.Base.apply(this, arguments);

        this.option('skip-welcome-message', {
            desc: 'Skips the welcome message',
            type: Boolean
        });

        this.option('skip-install-message', {
            desc: 'Skips the message after the installation of dependencies',
            type: Boolean
        });
    },

    initializing: function () {
        this.pkg = require('../../package.json');
    },

    askFor: function () {
        var done = this.async();

        if (!this.options['skip-welcome-message']) {
            this.log(yosay('Hello Code & Coffee! Included as standard in this front end generator is inuit.css.'));
        }

        var prompts = [{
            type: 'list',
            name: 'taskRunner',
            message: 'Which taskrunner would you like to use?',
            choices: [{
                name: 'Grunt.js',
                value: 'grunt',
                checked: true
            }, {
                name: 'Gulp.js',
                value: 'gulp'
            }]
        }, {
            type: 'checkbox',
            name: 'features',
            message: 'Would you like anything else?',
            choices: [{
                name: 'Sass',
                value: 'includeSass',
                checked: false
            },
            {
                name: 'Angular 2.0',
                value: 'includeAngular',
                checked: false
            }]
        }];

        this.prompt(prompts, function (answers) {
            var features = answers.features;

            function hasFeature(feat) {
                return features && features.indexOf(feat) !== -1;
            }

            this.includeSass = hasFeature('includeSass');
            this.includeAngular = hasFeature('includeAngular');
            this.taskRunner = answers.taskRunner;

            done();
        }.bind(this));
    },

    writing: {
        gulpfile: function () {
          if (this.taskRunner === 'gulp') {
            this.fs.copyTpl(
              this.templatePath('_gulpfile.js'),
              this.destinationPath('gulpfile.js'),
              {
                pkg: this.pkg,
                includeSass: this.includeSass
              }
            );
          }
        },

        gruntfile: function () {
            if (this.taskRunner === 'grunt') {
                this.fs.copyTpl(
                    this.templatePath('_Gruntfile.js'),
                    this.destinationPath('Gruntfile.js'),
                    {
                        pkg: this.pkg,
                        includeSass: this.includeSass
                    }
                );
            }
        },

        packageJSON: function () {
            this.fs.copyTpl(
                this.templatePath('_package.json'),
                this.destinationPath('package.json'),
                {
                    includeSass: this.includeSass,
                    taskRunner: this.taskRunner
                }
            )
        },

        git: function () {
            this.fs.copy(
                this.templatePath('gitignore'),
                this.destinationPath('.gitignore')
            );

            this.fs.copy(
                this.templatePath('gitattributes'),
                this.destinationPath('.gitattributes')
            );
        },

        bower: function () {
            var bowerJson = {
                name: _s.slugify(this.appname),
                private: true,
                dependencies: {}
            };

            bowerJson.dependencies['inuit.css'] = '~5.0.1';

            this.fs.writeJSON('bower.json', bowerJson);
            this.fs.copy(
                this.templatePath('bowerrc'),
                this.destinationPath('.bowerrc')
            );
        },

        editorConfig: function () {
            this.fs.copy(
                this.templatePath('editorconfig'),
                this.destinationPath('.editorconfig')
            );
        },

        scripts: function () {
            var script;

            if (this.includeAngular) {
                script = '_main-angular.js';
            } else {
                script = '_main.js';
            }
            this.fs.copyTpl(
                this.templatePath(script),
                this.destinationPath('app/scripts/main.js')
            );
        },

        styles: function () {
            var stylesheet, extension;

            if (this.includeSass) {
                stylesheet = '_main.scss';
                extension = '.scss';
            } else {
                stylesheet = '_main.css';
                extension = '.css';
            }

            this.fs.copyTpl(
                this.templatePath(stylesheet),
                this.destinationPath('app/styles/main' + extension)
            )
        },

        html: function () {
            this.fs.copyTpl(
                this.templatePath('_index.html'),
                this.destinationPath('app/index.html'),
                {
                    appname: this.appname,
                    taskrunner: this.taskRunner,
                    includeAngular: this.includeAngular,
                    includeSass: this.includeSass
                }
            );
        },

        robots: function () {
            this.fs.copy(
                this.templatePath('robots.txt'),
                this.destinationPath('app/robots.txt')
            );
        },

        misc: function () {
            mkdirp('app/images');
            mkdirp('app/fonts');
        }
    },

    install: function () {
        this.installDependencies({
            skipInstall: this.options['skip-install'],
            skipMessage: this.options['skip-install-message']
        });
    },

    end: function () {
        console.log('Done!');
    }
});

module.exports = CodeCoffeeGenerator;