/**
 * The main-angular.js file holds the functionality
 * to render out Angular components to showcase
 * its functionality, if that is chosen in the Yeoman
 * generator.
 * 
 * @author  Barry Hassan  29/08/2015
 */

'use strict';

/**
 * On DOM Ready
 */
document.addEventListener('DOMContentLoaded', function(event) { 

    console.log('Hello Code & Coffee! We\'re now using Angular 2.0');

    /**
     * JurassicPark is a constructor function that
     * holds the Angular annotation properties
     * that we use for rendering. This is a child
     * of the parent CodeCoffee component.
     */
    function JurassicPark(){}
    JurassicPark.annotations = [
        new angular.ComponentAnnotation({
            selector: 'dinosaur'
        }),
        new angular.ViewAnnotation({
            template: '<div>A simple note to say hi to Code & Coffee with Angular 2.0</div>'
        })
    ];

    /**
     * CodeCoffee is the parent constructor function
     * that holds the Angular annotations
     * that we use for rendering. This holds a 'dinosaur'
     * tag that tells the Jurassic Park component, this is
     * where I should be rendered.
     */
    function CodeCoffee(){}
    CodeCoffee.annotations = [
        new angular.ComponentAnnotation({
            selector: 'codecoffee'
        }),
        new angular.ViewAnnotation({
            directives: [JurassicPark],
            template: '<div><h1>Hello everyone</h1><dinosaur></dinosaur></div>'
        })
    ];

    /**
     * Root component bootstrap
     */
    angular.bootstrap(CodeCoffee);

});