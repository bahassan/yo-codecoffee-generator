/**
 * The main.js file holds a console log
 * to show that this template file
 * has been copied over successfully to
 * the target app.
 * 
 * @author  Barry Hassan  29/08/2015
 */

'use strict';

document.addEventListener('DOMContentLoaded', function(event) { 

    console.log('Hello Code & Coffee!');

});