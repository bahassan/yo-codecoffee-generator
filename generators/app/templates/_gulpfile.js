var gulp      = require('gulp');
var plumber   = require('gulp-plumber');
var sass      = require('gulp-sass');
var webserver = require('gulp-webserver');
var opn       = require('opn');

var server = {
    host: 'localhost',
    port: '8001'
}

gulp.task('sass', function () {
    gulp.src('./app/styles/**/*.scss')
        .pipe(plumber())
        .pipe(sass())
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./app/styles/'));
});

gulp.task('webserver', function() {
    gulp.src('app')
        .pipe(webserver({
        host:             server.host,
        port:             server.port,
        livereload:       true,
        directoryListing: false
    }));
});

gulp.task('openbrowser', function() {
    opn( 'http://' + server.host + ':' + server.port );
});

gulp.task('watch', function(){
    gulp.watch('./app/styles/**/*.scss', ['sass']);
});

gulp.task('build', ['sass']);

gulp.task('serve', ['build', 'webserver', 'watch', 'openbrowser']);
